<img src="assets/spotted hiena.jpg" alt="Spotted hiena logo" style="height: 100px; width:100px;"/>

# Trading automatico aplicado al mercado de Futuros perpetuos en criptomonedas
Bienvenido! Esta carpeta contiene el proyecto de titulo de Andres Romo.
El proyecto consiste en la optimizacion de un modelo de trading automatico para operar sobre el futuros de criptomonedas.
En esta carpeta encontrara todos los archivos necesarios para ejecutar la optimizacion, pero la teoria detras del modelo se encuentra en el informe de titulo. Aqui solamente esta el codigo con la implementacion.

# Documentos en esta carpeta

En esta carpeta se encuentran los siguientes archivos:

1. Archivos con implementacion de metaheuristicas
    1. PSO6.ipynb:  Implementa Particle Swarm Optimization
    2. SHO6.ipynb:  Implementa Spotted Hyena Optimizer
    3. LB2-6.ipynb: Implementa Linear Based Load Balancer
2. btcusdt-markprice-15m.csv:   Datos historicos de BTCUSDT (instrumento financiero)
3. Dataset.ipynb:   Clase encargada de subdivir los datos, ordenarlos y calcular las herramientas estadistica apropiadas.
4. FuncAuxOptim.opynb:  Funciones auxiliares comunes a todas las metaheuristicas.
5. Simulacion: Modelo de trading que implementa la estrategia detallada en el informe de titulo.
6. main.ipynb: Archivo principal. Este es el archivo que se debe abrir y ejecutar.

# Instalacion
El software fue programado en un entorno linux (Ubuntu 22.04.2 LTS). Se recomienda utilizar un entorno linux similar.
El software recomendado para ejecutar el codigo es:

<ul>
    <li>Visual Studio Code 1.78.0 o superior</li>
    <li>Python 3.10.6 o superior</li>
</ul>

Se recomienda crear un entorno virtual con visual studio code. Para ello, puede seguir las siguientes instrucciones

<ul>
    <li>Teclear el atajo ctrl+shfit+p. Esto deberia abrir la paleta de comandos de vscode.</li>
    <li>Buscar el comando: Python: Create Environment.</li>
    <li>Seleccionar venv como entorno virtual.</li>
</ul>

Luego, es necesario instalar las dependencias del proyecto, las cuales se encuentran especificadas en el archivo requirements.txt. Para instalarlas,

<ul>
    <li>Abra una termina en la carpeta del proyecto. En caso de estar utilizando un entorno virtual asegurese de tenerlo activado. Si no esta activado, se puede activar con <code>$ source .venv/bin/activate</code>, donde previamente vscode creo un entorno virtual en la carpeta .venv</li>
    <li>Escriba el comando <code>$ pip3 install -r requirements.txt</code></li>
</ul>


# Plan de pruebas

## 1. Introduccion

El plan de pruebas completo puede ejecutarse abriendo el archivo main.ipynb y ejecutando cada una de las celdas. Para esto, primero es necesario haber realizado la seccion de "Instalacion" de este archivo README.


## 2. Objetivos
El objetivo del plan de pruebas es realizar 3 "experimientos" abreviados para el modelo de trading propuesto en el archivo de Simulacion.ipynb y con los datos aportados por el modulo Dataset.ipynb. Se debe realizar 1 "experimento" abreviado por cada metaheuristica. Se define un experimento como:

- 12 soluciones distintas encontradas con la misma metaheuristica.
Y un experimiento abreviado como:
- 1 solucion encontrada por una metaheuristica

El resultado obtenido en el experimiento abreviado es 1 solucion aportada por una metaheuristica para el problema del trading con futuros en criptomonedas. Ese resultado vendra acompanado con dos valores adicionales:

1. Retorno de inversion (funcion objetivo) con los datos de optimizacion.
2. Retorno de inversion (funcion objetivo) con datos fuera del conjunto del optimizacion.

En teoria, si se utilizaron esos parametros para el algorimo de trading operando en un entorno real (en la plataforma binanceFutures), se esperaria obtener un retorno similar al retorno de inversion de test (con datos fuera del conjunto de optimizacion).

## 3. Procedimiento

1. Seguir el proceso de instalacion e instalar las dependencias necesarias
2. Ejecutar cada celda en el archivo main.ipynb

## 4. Criterio de aceptaciones

La prueba se considerar exitosa si al ejecutar cada celda (experimeto_pso, experimento_sho y experimento_lb2) se obtiene una solucion al problema del trading utilizando la estrategia de 2-sma. La solucion se presentara en forma de tabla (dataframe de pandas) donde se encuentran los siguientes valores:

- sma_slow: Cantidad de intervalos de tiempo para calcular la media movil lenta.
- sma_fast: Cantidad de intervalos de tiempo para calcular la media movil rapida.
- stop_loss_long: Porcentaje del maximo valor alcanzado para cancelar operaciones de tipo long.
- stop_loss_short: Porcentaje del minimo valor alcanzado para cancelar operaciones del tipo short.
- stop_win_long: Porcentaje del valor de entrada para finalizar operaciones tipo long.
- stop_win_short: Porcentaje del valor de entrada para finalizar operaciones tipo short.
- roi_train: Media geometrica del retorno de inversion obtenido en las simulaciones de optimizacion.
- roi_test: Media geometrica del retorno de inversion obtenido en las simulaciones de test.
- duration: Tiempo que tardo la optimizacion.





